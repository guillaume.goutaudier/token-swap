# Introduction

The objective of this project is to implement a token swap between Ethereum and Hyperledger Fabric. 

Here is a simple representation of what we want to achieve:
![swap.png](pics/swap.png)

The ERC20 interface is the following:

```java
contract ERC20Interface {
    function totalSupply() public constant returns (uint);
    function balanceOf(address tokenOwner) public constant returns (uint balance);
    function allowance(address tokenOwner, address spender) public constant returns (uint remaining);
    function transfer(address to, uint tokens) public returns (bool success);
    function approve(address spender, uint tokens) public returns (bool success);
    function transferFrom(address from, address to, uint tokens) public returns (bool success);

    event Transfer(address indexed from, address indexed to, uint tokens);
    event Approval(address indexed tokenOwner, address indexed spender, uint tokens);
}
```



# Setup of the Ethereum part
### Token implementation
We will use the Remix web IDE and the integrated JVM as our main Ethereum environment:
https://remix.ethereum.org/

Instead of reinventing the wheel, we will use the Open Zeppelin ERC20 contract as a starting point. 
Essentially we will follow the steps from the OpenZeppelin documentation on Tokens:
https://docs.openzeppelin.com/contracts/2.x/tokens

Remix has the capability to make direct imports from Github. This is what we will do:
```javascript
pragma solidity ^0.5.0;

import "github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/token/ERC20/ERC20.sol";
import "github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/token/ERC20/ERC20Detailed.sol";

contract GGOToken is ERC20, ERC20Detailed {
    constructor(uint256 initialSupply) ERC20Detailed("GuillaumeToken", "GGO", 0) public {
        _mint(msg.sender, initialSupply);
    }
}
```

In Remix, we can deploy this token on the JavaScript VM and mint an initial quantity of 100 tokens:
![remix.png](pics/remix.png)

The JavaScript VM comes with 5 predefined accounts that all have 100 Ethers. For our test purposes, we will arbitrarily consider that "Guillaume" is the owner of the 1st account and also the one who deploys the contract. We will also consider that "Alice" has the second account, and "Bob" the 3d one. So after deploy the contract, we will transfer 10 tokens to Alice.


### Safe implementation
For the creation of the safe, we will create a Smart Contract with the following functions:
- setupTransfer() that will move funds from the one token owner (e.g. Alice) into an escrow account (the Smart Contract address), and record a challenge that can only be solved by the knowledge of a secret
- executeTransfer() that will execute the transfer if the correct secret is provided
- cancelTransfer() that will move back the funds to the token owner after a specifc time period has elapsed

You can have a look at `Safe.sol` to review the implementation. Note that to deploy the contract we need to pass the arguments this way:
```
"0x692a70D2e424a56D2C6C27aA97D1a86395877b3A","0x14723A09ACff6D2A60DcdF7aA4AFf308FDDC160C","0x4B0897b0513fdC7C541B6d9D7E929C4e5364D2dB"
```

Likewise, the setupTransfer function should be called with these arguments format:
```
5,"0x25116287a1b258617d968f41e4d3aa85990958d404d4afeef9d2d674aa552d15"
```

For testing purposes we can use the following challenge/response:
```
challenge = "0x25116287a1b258617d968f41e4d3aa85990958d404d4afeef9d2d674aa552d15"
response (keccak256) = "0x7465737400000000000000000000000000000000000000000000000000000000"
```


# Setup of the Hyperledger Fabric part 
### Hyerledger Fabric Environment
We will use the Mock Shim library to avoid deploying a complete Hyperledger Fabric blockchain. For an introduction to the Mock Shim library, please refer to this other project: https://gitlab.com/guillaume.goutaudier/mockshim

### Token implementation
A proper ERC20 ChainCode implementation in Go is available here:
https://github.com/s7techlab/cckit/tree/master/examples/erc20

In our case we will go for a minimalistic implementation, where we will only implement the transfer_from function that can be called from anyone. Identity validation can be implemented using the CID library. We have not made such an implementation because it is difficult to test (from or own experience the Mock Shim library does not seem to work well with the CID library yet).

We will also store the state of the Token smart contract will be stored on a JSON object:
```go
type Balance struct {
  Owner string `json:"owner"`
  Amount int `json:"amount"`
}

type TokenData struct {
  TotalSupply int `json:"totalsupply"`
  Symbol     string `json:"symbol"`
  Balances   []Balance `json:"balances"`
}
```

You can have a look at `Token.go` to review the implementation.

### Safe implementation
For the creation of the safe, we will create a chaincode with the following functions:
- setupTransfer() that will move funds from the one token owner (e.g. Bob) into an escrow account, and record a challenge that can only be solved by the knowledge of a secret
- executeTransfer() that will execute the transfer if the correct secret is provided
- cancelTransfer() that will move back the funds to the token owner after a specifc time period has elapsed


# Putting it all together


# TODO
- Drill of the complete swap
- Make a proper Hyperledger Fabric implementation using the CID library for controling identities.
- Security review.






