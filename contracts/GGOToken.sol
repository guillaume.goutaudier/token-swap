pragma solidity ^0.5.0;

import "github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/token/ERC20/ERC20.sol";
import "github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/token/ERC20/ERC20Detailed.sol";

contract GGOToken is ERC20, ERC20Detailed {
    constructor(uint256 initialSupply) ERC20Detailed("GuillaumeToken", "GGO", 0) public {
        _mint(msg.sender, initialSupply);
    }
}

