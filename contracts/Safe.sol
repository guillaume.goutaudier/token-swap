pragma solidity ^0.5.0;

import {GGOToken as GGOToken} from "./GGOToken.sol";

contract Safe {
    GGOToken ggo;
    address source;
    address destination;
    uint amount;
    uint validity;
    bool locked;
    bytes32 challenge;

    constructor(address _token_contract, address _source, address _destination) public {
        ggo = GGOToken(_token_contract);
        source = _source;
        destination = _destination;
        locked = false;
    }

    function setupTransfer(uint _amount, bytes32 _challenge) public returns (bool _locked) {
        require(msg.sender == source);
        challenge = _challenge;
        amount = _amount;
        validity = now + 10;
        ggo.transferFrom(source,address(this),_amount);
        locked = true;
        _locked = locked;
    }
    
    function executeTransfer(bytes32 _challenge) public returns (bytes32 _hash)  {
        require(msg.sender == destination);
        require(locked == true);
        _hash = keccak256(abi.encodePacked(_challenge));
        if ( _hash != challenge ) {
            revert();
        }
        ggo.transfer(destination,amount);
        locked = false;
    }
    
    function cancelTransfer() public returns (bool _locked)  {
        require(msg.sender == source);
        require(locked = true);
        require(now > validity);
        ggo.transfer(source,amount);
        amount = 0;
        locked = false;
        _locked = locked;
    }
    
    function getLockState() public view returns (bool _locked) {
        _locked = locked;
    }    
    
    function getValidity() public view returns (uint _validity) {
        _validity = validity;
    }   
    
    function getNow() public view returns (uint _now) {
        _now = now;
    }   
    
}

