package main

import (
  "fmt"
  "strconv"
  "encoding/json"
  "github.com/hyperledger/fabric/core/chaincode/shim"
  "github.com/hyperledger/fabric/protos/peer"
)

// AtomicSwap implements a chaincode that emulates an ERC20 token and a Safe for atomic swaps
type AtomicSwap struct {
}

func (t *AtomicSwap) Init(stub shim.ChaincodeStubInterface) peer.Response {
  // Prepare initial state
  initial_balance := Balance{Owner: "Guillaume", Amount: 100}
  initial_tokendata := TokenData{TotalSupply: 100, Symbol: "GGO", Balances: []Balance{initial_balance}}
  initial_tokendata_as_bytes, err := json.MarshalIndent(initial_tokendata,"","  ")
  if err != nil {
    fmt.Println(err)
    return shim.Error(fmt.Sprintf("Unable to generate initial Token state"))
  }

  initial_safedata := SafeData{SourceOwner: "", DestinationOwner: "", EscrowAccount: "Escrow", Amount: 0, ValidityTimestamp: 0, Challenge: "", State: "Unlocked"}
  initial_safedata_as_bytes, err := json.MarshalIndent(initial_safedata,"","  ")
  if err != nil {
    fmt.Println(err)
    return shim.Error(fmt.Sprintf("Unable to generate initial Safe state"))
  }

  // Record intial state
  err = stub.PutState("TokenData", initial_tokendata_as_bytes)
  if err != nil {
    fmt.Println(err)
    return shim.Error(fmt.Sprintf("Failed to record initial Token state"))
  }
  err = stub.PutState("SafeData", initial_safedata_as_bytes)
  if err != nil {
    fmt.Println(err)
    return shim.Error(fmt.Sprintf("Failed to record initial Safe state"))
  }

  fmt.Println("Token chaincode successfully initialized")
  fmt.Println("TokenData initial state:\n"+string(initial_tokendata_as_bytes))
  fmt.Println("SafeData initial state:\n"+string(initial_safedata_as_bytes))
  return shim.Success(nil)
}

func (t *AtomicSwap) Invoke(stub shim.ChaincodeStubInterface) peer.Response {
  fn, args := stub.GetFunctionAndParameters()
  var result string
  var err error
  switch fn {
  case "totalSupply":
    result, err = totalSupply(stub)
  case "balanceOf":
    result, err = balanceOf(stub,args[0])
  case "transferFrom":
    amount, conv_err := strconv.Atoi(args[2])
    if conv_err != nil {
        fmt.Println(conv_err)
        shim.Error(fmt.Sprintf("Invalid amount "+args[2]))
    }
    result, err = transferFrom(stub,args[0],args[1],amount)
  case "getSafeState":
    result, err = getSafeState(stub)
  case "setupTransfer":
   amount, conv_err := strconv.Atoi(args[2])
    if conv_err != nil {
        fmt.Println(conv_err)
        shim.Error(fmt.Sprintf("Invalid amount "+args[2]))
    }
   validity_period, conv_err2 := strconv.Atoi(args[3])
   if conv_err2 != nil {
        fmt.Println(conv_err2)
        shim.Error(fmt.Sprintf("Invalid validity period "+args[3]))
    }
    result, err = setupTransfer(stub, args[0], args[1], amount, validity_period, args[4])
  case "executeTransfer":
    result, err = executeTransfer(stub,args[0])
  default:
    return shim.Error(fmt.Sprintf("Invoked invalid function "+fn))
  }
  if err != nil {
    return shim.Error(err.Error())
  }
  return shim.Success([]byte(result))
}

func main() {
    if err := shim.Start(new(AtomicSwap)); err != nil {
            fmt.Printf("Error starting AtomicSwap chaincode: %s", err)
    }
}

