package main

import (
  "fmt"
  "testing"
  "github.com/hyperledger/fabric/core/chaincode/shim"
)

func TestInvokeGet(t *testing.T) {

  // Create a Stub for the ChainCode
  fmt.Println("Creating stub for the AtomicSwap chaincode")
  stub := shim.NewMockStub("mockStub", new(AtomicSwap))
  if stub == nil {
    t.Fatalf("MockStub creation failed")
  }

  // Initialize the ChainCode
  fmt.Println("Initializing chaincode")
  init_result := stub.MockInit("1", [][]byte{})
  if init_result.Status != shim.OK {
    t.Fatalf("Failed to initialize ChainCode")
  }
  fmt.Println("Chaincode successfully initialized")

  // Query totalSupply()
  fmt.Println("Querying totalSupply() function")
  invoke_result := stub.MockInvoke("2", [][]byte{[]byte("totalSupply")})
  if invoke_result.Status != shim.OK {
    t.Fatalf("Failed to invoke ChainCode")
  }

  // Query balanceOf("Guillaume")
  fmt.Println("Querying balanceOf(\"Guillaume\")")
  invoke_result = stub.MockInvoke("3", [][]byte{[]byte("balanceOf"),[]byte("Guillaume")})
  if invoke_result.Status != shim.OK {
    t.Fatalf("Failed to invoke ChainCode")
  }

  // Query balanceOf("Alice")
  fmt.Println("Querying balanceOf(\"Alice\")")
  invoke_result = stub.MockInvoke("4", [][]byte{[]byte("balanceOf"),[]byte("Alice")})
  if invoke_result.Status != shim.OK {
    t.Fatalf("Failed to invoke ChainCode")
  }

  // Transfer 20 GGO tokens from "Guillaume" to "Alice"
  fmt.Println("Calling transferFrom(\"Guillaume\",\"Alice\",20)")
  invoke_result = stub.MockInvoke("5", [][]byte{[]byte("transferFrom"),[]byte("Guillaume"),[]byte("Alice"),[]byte("20")})
  if invoke_result.Status != shim.OK {
    t.Fatalf("Failed to invoke ChainCode")
  }

  // Check Token state
  tokendata_as_bytes, err := stub.GetState("TokenData")
  if err != nil {
    fmt.Println(err)
    t.Errorf("Failed to get state for key TokenData")
  } else if tokendata_as_bytes == nil {
    t.Errorf("No value for key TokenData")
  }
  fmt.Println("Updated state for TokenData: "+string(tokendata_as_bytes))

  // Check Safe State
  fmt.Println("Calling getSafeState()")
  invoke_result = stub.MockInvoke("6", [][]byte{[]byte("getSafeState")})
  if invoke_result.Status != shim.OK {
    t.Fatalf("Failed to invoke ChainCode")
  }

  // Setup a transfer
  fmt.Println("Calling setupTransfer()")
  invoke_result = stub.MockInvoke("7", [][]byte{[]byte("setupTransfer"),[]byte("Alice"),[]byte("Bob"),[]byte("5"),[]byte("60"),[]byte("f5a5207a8729b1f709cb710311751eb2fc8acad5a1fb8ac991b736e69b6529a3")})
  if invoke_result.Status != shim.OK {
    t.Fatalf("Failed to invoke ChainCode")
  }

  // Check Safe State
  fmt.Println("Calling getSafeState()")
  invoke_result = stub.MockInvoke("7", [][]byte{[]byte("getSafeState")})
  if invoke_result.Status != shim.OK {
    t.Fatalf("Failed to invoke ChainCode")
  }

  // Check Token state
  tokendata_as_bytes, err = stub.GetState("TokenData")
  if err != nil {
    fmt.Println(err)
    t.Errorf("Failed to get state for key TokenData")
  } else if tokendata_as_bytes == nil {
    t.Errorf("No value for key TokenData")
  }
  fmt.Println("Updated state for TokenData: "+string(tokendata_as_bytes))

  // Execute a transfer
  fmt.Println("Calling executeTransfer()")
  invoke_result = stub.MockInvoke("8", [][]byte{[]byte("executeTransfer"),[]byte("secret")})
  if invoke_result.Status != shim.OK {
    t.Fatalf("Failed to invoke ChainCode")
  }

  // Check Token state
  tokendata_as_bytes, err = stub.GetState("TokenData")
  if err != nil {
    fmt.Println(err)
    t.Errorf("Failed to get state for key TokenData")
  } else if tokendata_as_bytes == nil {
    t.Errorf("No value for key TokenData")
  }
  fmt.Println("Updated state for TokenData: "+string(tokendata_as_bytes))

}


