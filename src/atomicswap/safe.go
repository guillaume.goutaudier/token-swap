package main

import (
  "encoding/json"
  "encoding/hex"
  "fmt"
  "golang.org/x/crypto/sha3"
  "github.com/hyperledger/fabric/core/chaincode/shim"
)

type SafeData struct {
  SourceOwner string `json:"source_owner"`
  DestinationOwner string `json:"destination_owner"`
  EscrowAccount string `json:"escrow_account"`
  Amount int `json:"amount"`
  ValidityTimestamp int64 `json:"validity_timestamp"`
  Challenge string `json:"challenge"`
  State string `json:"safe_state"`
}

// Helper function
func getSafeData(stub shim.ChaincodeStubInterface, safedata *SafeData) (error) {
  safedata_as_bytes, err := stub.GetState("SafeData")
  if err != nil {
    fmt.Println(err)
    return fmt.Errorf("Failed to get state for key SafeData")
  }
  if safedata_as_bytes == nil {
    return fmt.Errorf("No value for key SafeData")
  }
  err = json.Unmarshal(safedata_as_bytes, &safedata)
  if err != nil {
    fmt.Println(err)
    return fmt.Errorf("Failed to Unmarshal SafeData")
  }
  return nil
}

func getSafeState(stub shim.ChaincodeStubInterface) (string, error) {
  // Get SafeData state
  var safedata SafeData
  err := getSafeData(stub,&safedata)
  if err != nil {
    return "", err
  }
  safedata_as_bytes, json_err := json.MarshalIndent(safedata,"","  ")
  if json_err != nil {
    fmt.Println(json_err)
    return "", fmt.Errorf("Unable to parse Safe state")
  }
  fmt.Printf("SafeData:\n"+string(safedata_as_bytes))
  return "", nil
}

func setupTransfer(stub shim.ChaincodeStubInterface, source string, destination string, amount int, validity_period int, challenge string) (string, error) {
  // Get SafeData state
  var safedata SafeData
  err := getSafeData(stub,&safedata)
  if err != nil {
    return "", err
  }

  // Get current timestamp
  tx_timestamp, tx_err := stub.GetTxTimestamp()
  if tx_err != nil {
    return "", tx_err
  }
  fmt.Println("tx_timestamp:", tx_timestamp)

  // Transfer tokens from source to escrow account
  fmt.Println("Transferring founds to escrow account...")
  _, transfer_error := transferFrom(stub, source, safedata.EscrowAccount, amount)
  if transfer_error != nil {
    return "", transfer_error
  }

  // Record updated state
  safedata.SourceOwner = source
  safedata.DestinationOwner = destination
  safedata.Amount = amount
  safedata.ValidityTimestamp = tx_timestamp.GetSeconds() + int64(validity_period)
  safedata.Challenge = challenge
  safedata.State = "Locked"

  safedata_as_bytes, marshal_err := json.MarshalIndent(safedata,"","  ")
  if marshal_err != nil {
    fmt.Println(marshal_err)
    return "", fmt.Errorf("Unable to Marshal safedata")
  }
  err = stub.PutState("SafeData", safedata_as_bytes)
  if err != nil {
    fmt.Println(err)
    return "", fmt.Errorf("Failed to record SafeData updated state")
  }
  return "", nil
}

func executeTransfer(stub shim.ChaincodeStubInterface, secret string) (string, error) {
  // Get SafeData state
  var safedata SafeData
  err := getSafeData(stub,&safedata)
  if err != nil {
    return "", err
  }

  // Get current timestamp
  tx_timestamp, tx_err := stub.GetTxTimestamp()
  if tx_err != nil {
    return "", tx_err
  }

  if tx_timestamp.GetSeconds() < safedata.ValidityTimestamp {
    fmt.Println("Timestamp still valid!")
  } else {
    fmt.Println("Error: timestamp not valid anymore")
    return "", fmt.Errorf("Can not execute transfer because validity period is expired")
  }

  fmt.Println("Checking whether provided secret solves the challenge")

  hash := sha3.New256()
  var buf []byte
  hash.Write([]byte(secret))
  buf = hash.Sum(nil)
  fmt.Printf("SHA3 of secret: %s\n", hex.EncodeToString(buf))

  if hex.EncodeToString(buf) == safedata.Challenge {
    fmt.Println("secret has been found: "+secret)
    fmt.Println("Transfering tokens to target")
     _, transfer_error := transferFrom(stub, safedata.EscrowAccount, safedata.DestinationOwner, safedata.Amount)
    if transfer_error != nil {
      return "", transfer_error
    }
  }
  safedata.State = "UnLocked"

  // Saving SafeData
  safedata_as_bytes, marshal_err := json.MarshalIndent(safedata,"","  ")
  if marshal_err != nil {
    fmt.Println(marshal_err)
    return "", fmt.Errorf("Unable to Marshal safedata")
  }
  err = stub.PutState("SafeData", safedata_as_bytes)
  if err != nil {
    fmt.Println(err)
    return "", fmt.Errorf("Failed to record SafeData updated state")
  }

  return "", nil
}



