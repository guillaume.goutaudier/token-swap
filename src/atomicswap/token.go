package main

import (
  "fmt"
  "encoding/json"
  "github.com/hyperledger/fabric/core/chaincode/shim"
)

type Balance struct {
  Owner string `json:"owner"`
  Amount int `json:"amount"`
}

type TokenData struct {
  TotalSupply int `json:"totalsupply"`
  Symbol     string `json:"symbol"`
  Balances   []Balance `json:"balances"`
}

// Helper function
func getTokenData(stub shim.ChaincodeStubInterface, tokendata *TokenData) (error) {
  tokendata_as_bytes, err := stub.GetState("TokenData")
  if err != nil {
    fmt.Println(err)
    return fmt.Errorf("Failed to get state for key TokenData")
  }
  if tokendata_as_bytes == nil {
    return fmt.Errorf("No value for key TokenData")
  }
  err = json.Unmarshal(tokendata_as_bytes, &tokendata)
  if err != nil {
    fmt.Println(err)
    return fmt.Errorf("Failed to Unmarshal TokenData")
  }
  return nil
}

func totalSupply(stub shim.ChaincodeStubInterface) (string, error) {
  // Get TokenData state
  var tokendata TokenData
  err := getTokenData(stub,&tokendata)
  if err != nil {
    return "", err
  }

  // Check total supply
  fmt.Printf("Total supply: %d\n",tokendata.TotalSupply)
  return fmt.Sprintf("%d",tokendata.TotalSupply), nil
}

func balanceOf(stub shim.ChaincodeStubInterface, owner string) (string, error) {
  // Get TokenData state
  var tokendata TokenData
  err := getTokenData(stub,&tokendata)
  if err != nil {
    return "", err
  }

  // Check Balance
  fmt.Println("Checking the balance of owner "+owner)
  for i := range tokendata.Balances {
    if tokendata.Balances[i].Owner == owner {
      fmt.Printf("Owner %s has %d tokens\n",tokendata.Balances[i].Owner,tokendata.Balances[i].Amount)
      return fmt.Sprintf("%d", tokendata.Balances[i].Amount), nil
    }
  }
  fmt.Printf("Owner %s does not have any token\n",owner)
  return "0", nil
}

func transferFrom(stub shim.ChaincodeStubInterface, from string, to string, amount int) (string, error) {
  // Get TokenData state
  var tokendata TokenData
  err := getTokenData(stub,&tokendata)
  if err != nil {
    return "", err
  }

  // Proceed with the transfer
  fmt.Println("Checking there are sufficient funds on the balance of "+from)
  for i := range tokendata.Balances {
    if tokendata.Balances[i].Owner == from {
      fmt.Printf("Owner %s has %d tokens\n",tokendata.Balances[i].Owner,tokendata.Balances[i].Amount)
      if tokendata.Balances[i].Amount >= amount {
        fmt.Printf("This is sufficient to pay %d to %s\n",amount,to)
        tokendata.Balances[i].Amount = tokendata.Balances[i].Amount - amount
        var destination_balance_exists bool = false
        for j := range tokendata.Balances {
          if tokendata.Balances[j].Owner == to {
            tokendata.Balances[j].Amount = tokendata.Balances[j].Amount + amount
            destination_balance_exists = true
            break
          }
        }
        if destination_balance_exists == false {
          tokendata.Balances = append(tokendata.Balances, Balance{Owner: to, Amount: amount})
        }
      } else  {
        fmt.Printf("This is not sufficient to pay %d to %s\n",amount,to)
        return "", fmt.Errorf("Unsufficient funds")
      }
    }
  }

  // Update TokenData state
  tokendata_as_bytes, marshal_err := json.MarshalIndent(tokendata,"","  ")
  if marshal_err != nil {
    fmt.Println(marshal_err)
    return "", fmt.Errorf("Unable to Marshal tokendata")
  }
  err = stub.PutState("TokenData", tokendata_as_bytes)
  if err != nil {
    fmt.Println(err)
    return "", fmt.Errorf("Failed to record TokenData updated state")
  }

  return "Successful transfer", nil
}


